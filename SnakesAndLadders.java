package com.assessment;

public class SnakeAndLadders {

	public static void main(String[] args) {

		Player A = new Player();
		Player B = new Player(18);

		System.out.println(getgamestatus(A, B));
		A.dicethrown();
		B.dicethrown();

		System.out.println(getgamestatus(A, B));
	}

	public static String getgamestatus(Player player1, Player player2) {
		String str = "";
		if (player1.getCurrentstatus() >= 100)
			str = "Player1 wins\n Game over";
		else if (player2.getCurrentstatus() >= 100)
			str = str + "Player2 wins\n Game over";
		else
			str = str + "Current status of players is at player1 is at " + player1.getCurrentstatus()
					+ ", player2 is at " + player2.getCurrentstatus() + "!";

		return str;
	}

}
