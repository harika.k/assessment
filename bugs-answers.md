Please type your answers out here

1.1.1 First repeated character = a
1.1.2 First repeated character = o
1.1.3 No repeated character.
1.1.4 No repeated character.

1.2.1 "aardvark" First repeated character = a
1.2.2 "roommate" First repeated character = o
1.2.3 "mate" java.lang.StringIndexOutOfBoundsException: String index out of range: 4
1.2.4 "test" java.lang.StringIndexOutOfBoundsException: String index out of range: 4

1.3   Yes, there is bug 'firstRepeatedCharacter`we get exception is java.lang.StringIndexOutOfBoundsException

		public char firstRepeatedCharacter()
	   {
	      for (int i = 0; i < (word.length())-1; i++)
	      {
	         char ch = word.charAt(i);
	         if (ch == word.charAt(i + 1))
	            return ch;
	      }
	      return 0;
	   }

1.4     If we pass 'null' for string then it will raises null pointer exception. to solve this we need you exception handling with 		try catch or we check null condition by using if as follows:
			
			public char firstRepeatedCharacter()
		   {
			   if(word!=null)
			   {
			      for (int i = 0; i < (word.length())-1; i++)
			      {
			         char ch = word.charAt(i);
			         if (ch == word.charAt(i + 1))
			            return ch;
			      }
			   }
		      return 0;
		   }
		

2.1.1 "missisippi" first multiple character = i
2.1.2 no multiple character
2.1.3 no multiple character

2.2.1 First multiple character 'm'
2.2.2 First multiple character 'm'
2.2.3 First multiple character 't'

2.3 Yes, there are bugs in the code as it will not giving output correctly.

		public char firstMultipleCharacter()
	   {
	      for (int i = 0; i < word.length(); i++)
	      {
	         char ch = word.charAt(i);
	         if (find(ch, i) >= 0)
	            return ch;
	      }
	      return 0;
	   }

	   private int find(char c, int pos)
	   {
	      for (int i = pos+1; i < word.length(); i++)
	      {
	         if (word.charAt(i) == c) 
	         {
	            return i;
	         }
	      }
	      return -1;
	   }
		
3.1.1 repeated characters 2
3.1.2 no repeated characters
3.1.3  repeated characters 4

3.2.1 "missisippi" 2 repeated characters.
3.2.2 "test" 0 repeated characters.
3.2.3 "aabbcdaaaabb" 3 repeated characters.

3.4  Yes, there is a bug it's not give proper output.

		public int countRepeatedCharacters()
	   {
	      int c = 0;
	      for (int i = 0; i < word.length() - 1; i++)		// changed i=o
	      {
	         if (word.charAt(i) == word.charAt(i + 1)) 
	         {
	            if ( i==0 || word.charAt(i - 1) != word.charAt(i)) // starts i =0;
	               c++;
	         }
	      }     
	      return c;
	   }

4.   to debug the code used breakpoints
5.    [Answer Here]