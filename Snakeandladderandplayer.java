package com.assesment.test;

public class Snakeandladderandplayer {

	private static Integer firststartpoint = 16;
	private static Integer firstendpoint = 6;
	private static Integer secondstartpoint = 46;
	private static Integer secondendpoint = 25;
	private static Integer thirdstartpoint = 49;
	private static Integer thirdendpoint = 11;
	private static Integer fourthstartpoint = 62;
	private static Integer fourthendpoint = 19;
	private static Integer fifthstartpoint = 64;
	private static Integer fifthendpoint = 60;
	private static Integer sixstartpoint = 74;
	private static Integer sixendpoint = 53;
	private static Integer sevenstartpoint = 89;
	private static Integer sevenendpoint = 68;
	private static Integer eightstartpoint = 92;
	private static Integer eightendpoint = 88;
	private static Integer ninestartpoint = 95;
	private static Integer nineendpoint = 75;
	private static Integer tenstartpoint = 99;
	private static Integer tenendpoint = 80;
	private static Integer ladderfirststartpoint = 2;
	private static Integer ladderfirstendpoint = 38;
	private static Integer laddersecondstartpoint = 7;
	private static Integer laddersecondendpoint = 14;
	private static Integer ladderthirdstartpoint = 8;
	private static Integer ladderthirdendpoint = 31;
	private static Integer ladderfourthstartpoint = 15;
	private static Integer ladderfourthendpoint = 26;
	private static Integer ladderfifthstartpoint = 21;
	private static Integer ladderfifthendpoint = 42;
	private static Integer laddersixthstartpoint = 28;
	private static Integer laddersixthendpoint = 84;
	private static Integer laddersevenstartpoint = 36;
	private static Integer laddersevenendpoint = 44;
	private static Integer laddereightstartpoint = 51;
	private static Integer laddereightendpoint = 67;
	private static Integer ladderninestartpoint = 78;
	private static Integer laddernineendpoint = 98;
	private static Integer laddertenstartpoint = 71;
	private static Integer laddertenendpoint = 91;
	private static Integer ladderelevenstartpoint = 87;
	private static Integer ladderelevenendpoint = 94;

	public static Integer getFirststartpoint() {
		return firststartpoint;
	}

	public static void setFirststartpoint(Integer firststartpoint) {
		Snakeandladderandplayer.firststartpoint = firststartpoint;
	}

	public static Integer getFirstendpoint() {
		return firstendpoint;
	}

	public static void setFirstendpoint(Integer firstendpoint) {
		Snakeandladderandplayer.firstendpoint = firstendpoint;
	}

	public static Integer getSecondstartpoint() {
		return secondstartpoint;
	}

	public static void setSecondstartpoint(Integer secondstartpoint) {
		Snakeandladderandplayer.secondstartpoint = secondstartpoint;
	}

	public static Integer getSecondendpoint() {
		return secondendpoint;
	}

	public static void setSecondendpoint(Integer secondendpoint) {
		Snakeandladderandplayer.secondendpoint = secondendpoint;
	}

	public static Integer getThirdstartpoint() {
		return thirdstartpoint;
	}

	public static void setThirdstartpoint(Integer thirdstartpoint) {
		Snakeandladderandplayer.thirdstartpoint = thirdstartpoint;
	}

	public static Integer getThirdendpoint() {
		return thirdendpoint;
	}

	public static void setThirdendpoint(Integer thirdendpoint) {
		Snakeandladderandplayer.thirdendpoint = thirdendpoint;
	}

	public static Integer getFourthstartpoint() {
		return fourthstartpoint;
	}

	public static void setFourthstartpoint(Integer fourthstartpoint) {
		Snakeandladderandplayer.fourthstartpoint = fourthstartpoint;
	}

	public static Integer getFourthendpoint() {
		return fourthendpoint;
	}

	public static void setFourthendpoint(Integer fourthendpoint) {
		Snakeandladderandplayer.fourthendpoint = fourthendpoint;
	}

	public static Integer getFifthstartpoint() {
		return fifthstartpoint;
	}

	public static void setFifthstartpoint(Integer fifthstartpoint) {
		Snakeandladderandplayer.fifthstartpoint = fifthstartpoint;
	}

	public static Integer getFifthendpoint() {
		return fifthendpoint;
	}

	public static void setFifthendpoint(Integer fifthendpoint) {
		Snakeandladderandplayer.fifthendpoint = fifthendpoint;
	}

	public static Integer getSixstartpoint() {
		return sixstartpoint;
	}

	public static void setSixstartpoint(Integer sixstartpoint) {
		Snakeandladderandplayer.sixstartpoint = sixstartpoint;
	}

	public static Integer getSixendpoint() {
		return sixendpoint;
	}

	public static void setSixendpoint(Integer sixendpoint) {
		Snakeandladderandplayer.sixendpoint = sixendpoint;
	}

	public static Integer getSevenstartpoint() {
		return sevenstartpoint;
	}

	public static void setSevenstartpoint(Integer sevenstartpoint) {
		Snakeandladderandplayer.sevenstartpoint = sevenstartpoint;
	}

	public static Integer getSevenendpoint() {
		return sevenendpoint;
	}

	public static void setSevenendpoint(Integer sevenendpoint) {
		Snakeandladderandplayer.sevenendpoint = sevenendpoint;
	}

	public static Integer getEightstartpoint() {
		return eightstartpoint;
	}

	public static void setEightstartpoint(Integer eightstartpoint) {
		Snakeandladderandplayer.eightstartpoint = eightstartpoint;
	}

	public static Integer getEightendpoint() {
		return eightendpoint;
	}

	public static void setEightendpoint(Integer eightendpoint) {
		Snakeandladderandplayer.eightendpoint = eightendpoint;
	}

	public static Integer getNinestartpoint() {
		return ninestartpoint;
	}

	public static void setNinestartpoint(Integer ninestartpoint) {
		Snakeandladderandplayer.ninestartpoint = ninestartpoint;
	}

	public static Integer getNineendpoint() {
		return nineendpoint;
	}

	public static void setNineendpoint(Integer nineendpoint) {
		Snakeandladderandplayer.nineendpoint = nineendpoint;
	}

	public static Integer getTenstartpoint() {
		return tenstartpoint;
	}

	public static void setTenstartpoint(Integer tenstartpoint) {
		Snakeandladderandplayer.tenstartpoint = tenstartpoint;
	}

	public static Integer getTenendpoint() {
		return tenendpoint;
	}

	public static void setTenendpoint(Integer tenendpoint) {
		Snakeandladderandplayer.tenendpoint = tenendpoint;
	}

	public static Integer getLadderfirststartpoint() {
		return ladderfirststartpoint;
	}

	public static void setLadderfirststartpoint(Integer ladderfirststartpoint) {
		Snakeandladderandplayer.ladderfirststartpoint = ladderfirststartpoint;
	}

	public static Integer getLadderfirstendpoint() {
		return ladderfirstendpoint;
	}

	public static void setLadderfirstendpoint(Integer ladderfirstendpoint) {
		Snakeandladderandplayer.ladderfirstendpoint = ladderfirstendpoint;
	}

	public static Integer getLaddersecondstartpoint() {
		return laddersecondstartpoint;
	}

	public static void setLaddersecondstartpoint(Integer laddersecondstartpoint) {
		Snakeandladderandplayer.laddersecondstartpoint = laddersecondstartpoint;
	}

	public static Integer getLaddersecondendpoint() {
		return laddersecondendpoint;
	}

	public static void setLaddersecondendpoint(Integer laddersecondendpoint) {
		Snakeandladderandplayer.laddersecondendpoint = laddersecondendpoint;
	}

	public static Integer getLadderthirdstartpoint() {
		return ladderthirdstartpoint;
	}

	public static void setLadderthirdstartpoint(Integer ladderthirdstartpoint) {
		Snakeandladderandplayer.ladderthirdstartpoint = ladderthirdstartpoint;
	}

	public static Integer getLadderthirdendpoint() {
		return ladderthirdendpoint;
	}

	public static void setLadderthirdendpoint(Integer ladderthirdendpoint) {
		Snakeandladderandplayer.ladderthirdendpoint = ladderthirdendpoint;
	}

	public static Integer getLadderfourthstartpoint() {
		return ladderfourthstartpoint;
	}

	public static void setLadderfourthstartpoint(Integer ladderfourthstartpoint) {
		Snakeandladderandplayer.ladderfourthstartpoint = ladderfourthstartpoint;
	}

	public static Integer getLadderfourthendpoint() {
		return ladderfourthendpoint;
	}

	public static void setLadderfourthendpoint(Integer ladderfourthendpoint) {
		Snakeandladderandplayer.ladderfourthendpoint = ladderfourthendpoint;
	}

	public static Integer getLadderfifthstartpoint() {
		return ladderfifthstartpoint;
	}

	public static void setLadderfifthstartpoint(Integer ladderfifthstartpoint) {
		Snakeandladderandplayer.ladderfifthstartpoint = ladderfifthstartpoint;
	}

	public static Integer getLadderfifthendpoint() {
		return ladderfifthendpoint;
	}

	public static void setLadderfifthendpoint(Integer ladderfifthendpoint) {
		Snakeandladderandplayer.ladderfifthendpoint = ladderfifthendpoint;
	}

	public static Integer getLaddersixthstartpoint() {
		return laddersixthstartpoint;
	}

	public static void setLaddersixthstartpoint(Integer laddersixthstartpoint) {
		Snakeandladderandplayer.laddersixthstartpoint = laddersixthstartpoint;
	}

	public static Integer getLaddersixthendpoint() {
		return laddersixthendpoint;
	}

	public static void setLaddersixthendpoint(Integer laddersixthendpoint) {
		Snakeandladderandplayer.laddersixthendpoint = laddersixthendpoint;
	}

	public static Integer getLaddersevenstartpoint() {
		return laddersevenstartpoint;
	}

	public static void setLaddersevenstartpoint(Integer laddersevenstartpoint) {
		Snakeandladderandplayer.laddersevenstartpoint = laddersevenstartpoint;
	}

	public static Integer getLaddersevenendpoint() {
		return laddersevenendpoint;
	}

	public static void setLaddersevenendpoint(Integer laddersevenendpoint) {
		Snakeandladderandplayer.laddersevenendpoint = laddersevenendpoint;
	}

	public static Integer getLaddereightstartpoint() {
		return laddereightstartpoint;
	}

	public static void setLaddereightstartpoint(Integer laddereightstartpoint) {
		Snakeandladderandplayer.laddereightstartpoint = laddereightstartpoint;
	}

	public static Integer getLaddereightendpoint() {
		return laddereightendpoint;
	}

	public static void setLaddereightendpoint(Integer laddereightendpoint) {
		Snakeandladderandplayer.laddereightendpoint = laddereightendpoint;
	}

	public static Integer getLadderninestartpoint() {
		return ladderninestartpoint;
	}

	public static void setLadderninestartpoint(Integer ladderninestartpoint) {
		Snakeandladderandplayer.ladderninestartpoint = ladderninestartpoint;
	}

	public static Integer getLaddernineendpoint() {
		return laddernineendpoint;
	}

	public static void setLaddernineendpoint(Integer laddernineendpoint) {
		Snakeandladderandplayer.laddernineendpoint = laddernineendpoint;
	}

	public static Integer getLaddertenstartpoint() {
		return laddertenstartpoint;
	}

	public static void setLaddertenstartpoint(Integer laddertenstartpoint) {
		Snakeandladderandplayer.laddertenstartpoint = laddertenstartpoint;
	}

	public static Integer getLaddertenendpoint() {
		return laddertenendpoint;
	}

	public static void setLaddertenendpoint(Integer laddertenendpoint) {
		Snakeandladderandplayer.laddertenendpoint = laddertenendpoint;
	}

	public static Integer getLadderelevenstartpoint() {
		return ladderelevenstartpoint;
	}

	public static void setLadderelevenstartpoint(Integer ladderelevenstartpoint) {
		Snakeandladderandplayer.ladderelevenstartpoint = ladderelevenstartpoint;
	}

	public static Integer getLadderelevenendpoint() {
		return ladderelevenendpoint;
	}

	public static void setLadderelevenendpoint(Integer ladderelevenendpoint) {
		Snakeandladderandplayer.ladderelevenendpoint = ladderelevenendpoint;
	}

	private Integer currentstatus;
	private Integer previousstatus;

	public Integer getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(Integer currentstatus) {
		this.currentstatus = currentstatus;
	}

	public Integer getPreviousstatus() {
		return previousstatus;
	}

	public void setPreviousstatus(Integer previousstatus) {
		this.previousstatus = previousstatus;
	}

	public void startGame() {
		currentstatus = 0;
	}

	public void dicethrown() {
		Integer[] dicevalues = new Integer[2];
		dicevalues[0] = Dice.diceValue();
		dicevalues[1] = Dice.diceValue();
		Integer previousval = 0;
		previousval = previousval + dicevalues[0] + dicevalues[1];
		while (dicevalues[0] == dicevalues[1]) {
			previousval = previousval + dicevalues[0] + dicevalues[1];
			dicevalues[0] = Dice.diceValue();
			dicevalues[1] = Dice.diceValue();
		}

		currentstatus = Board.getvalue(previousval);

	}

}
